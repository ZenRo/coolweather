package roman.coolweather.di

import androidx.room.Room
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import roman.coolweather.data.WeatherRepository
import roman.coolweather.data.api.WeatherApi
import roman.coolweather.data.api.core.WeatherClient
import roman.coolweather.data.db.AppDatabase
import roman.coolweather.ui.main.MainViewModel
import roman.coolweather.util.PreferencesManager

val mainModule = module {
    single { WeatherClient().requests }
    single { WeatherApi(get()) }
    single { WeatherRepository(get(), get()) }
    single { PreferencesManager(get()) }
    single(createOnStart = true) { Room.databaseBuilder(get(), AppDatabase::class.java, "app.db").build() }

    // ViewModels
    viewModel { MainViewModel(get()) }
}