package roman.coolweather.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_weather.view.*
import roman.coolweather.R
import roman.coolweather.data.db.entity.WeatherDB
import roman.coolweather.ui.main.OnWeatherEvent
import roman.coolweather.ui.map.MapInfo
import roman.coolweather.databinding.ItemWeatherBinding

class WeatherAdapter(private val listener: OnWeatherEvent) : RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {
    private var weatherList: ArrayList<WeatherDB> = arrayListOf()

    fun addWeather(weather: WeatherDB) {
        if (this.weatherList.contains(weather)) {
            this.weatherList.remove(weather)
        }
        this.weatherList.add(0, weather)

        notifyDataSetChanged()
    }

    fun deleteWeather(weather: WeatherDB) {
        this.weatherList.remove(weather)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemWeatherBinding = DataBindingUtil.inflate(inflater, R.layout.item_weather, parent, false)

        return WeatherViewHolder(binding.root, binding)
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.binding.weather = weatherList[position]
        holder.binding.executePendingBindings()
        holder.initClicks(weatherList[position])
    }

    override fun getItemCount(): Int = weatherList.size

    inner class WeatherViewHolder(itemView: View, val binding: ItemWeatherBinding): RecyclerView.ViewHolder(itemView) {

        fun initClicks(item: WeatherDB) {
            // Delete from DB button
            itemView.deleteImageView.visibility = View.GONE
            item.city?.let { city ->
                itemView.deleteImageView.visibility = View.VISIBLE
                itemView.deleteImageView.setOnClickListener {
                    listener.onDeleteCityClicked(city)
                    deleteWeather(item)
                }
            }

            // OnClick on item
            itemView.setOnClickListener {
                val mapInfo = MapInfo(item.name, item.lat, item.lon, item.temp)

                listener.onWeatherClicked(mapInfo)
            }
        }
    }
}