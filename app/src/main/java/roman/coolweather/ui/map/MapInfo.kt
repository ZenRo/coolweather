package roman.coolweather.ui.map

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MapInfo(val name: String,
                   val lat: Double,
                   val lon: Double,
                   val temp: String) : Parcelable