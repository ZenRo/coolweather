package roman.coolweather.ui.map

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import roman.coolweather.R

private const val EXTRA_MAP_INFO = "EXTRA_MAP_INFO"

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    private lateinit var mapInfo: MapInfo

    companion object {
        fun startActivity(context: Context, mapInfo: MapInfo) {
            val intent = Intent(context, MapsActivity::class.java)
            intent.putExtra(EXTRA_MAP_INFO, mapInfo)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        mapInfo = intent.getParcelableExtra(EXTRA_MAP_INFO)

        title = mapInfo.name
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker
        val point = LatLng(mapInfo.lat, mapInfo.lon)
        val temp = mapInfo.temp

        mMap.addMarker(MarkerOptions()
                .position(point)
                .title("${mapInfo.name} - ${if (temp.toDouble() >= 0) "+" else "-" }$temp ℃"))
            .showInfoWindow()
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 15F))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
