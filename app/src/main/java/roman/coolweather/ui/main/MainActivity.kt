package roman.coolweather.ui.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_add_city.view.*
import org.koin.android.ext.android.inject
import roman.coolweather.R
import roman.coolweather.data.db.entity.City
import roman.coolweather.ui.adapter.WeatherAdapter
import roman.coolweather.ui.map.MapInfo
import roman.coolweather.ui.map.MapsActivity
import roman.coolweather.util.PreferencesManager

private const val LOCATION_REQUEST_CODE = 100

class MainActivity : AppCompatActivity(), OnWeatherEvent {
    private val viewModel: MainViewModel by inject()
    private val preferencesManager: PreferencesManager by inject()
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var adapter: WeatherAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup default city
        if (preferencesManager.isFirstStart()) {
            preferencesManager.setFirstStart(false)

            val city = City(name = "Kyiv")
            viewModel.insertCity(city)
        }

        initViews()
        initLocation() // fetch weather by current location
        initObservers()

        viewModel.fetchCities()
    }

    private fun initViews() {
        adapter = WeatherAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        fab.setOnClickListener { showAddCityDialog() }
    }

    private fun initObservers() {
        viewModel.weather.observe(this, Observer { weather ->
            adapter.addWeather(weather)
        })

        viewModel.error.observe(this, Observer { error ->
            Toast.makeText(this@MainActivity, error, Toast.LENGTH_LONG).show()
        })
    }

    override fun onWeatherClicked(mapInfo: MapInfo) {
        // OnClick
        MapsActivity.startActivity(this, mapInfo)
    }

    override fun onDeleteCityClicked(city: City) {
        viewModel.deleteCity(city)
    }

    /////////////////// LOCATION /////////////////
    private fun initLocation() {
        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST_CODE)
            return
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            //Toast.makeText(this@MainActivity, "Location: ${location.longitude} , ${location.latitude}", Toast.LENGTH_LONG).show()
            viewModel.fetchWeatherAt(location.latitude.toString(), location.longitude.toString())
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initLocation()
                }
            }
        }
    }
    /////////////////////////////////////////////

    // Input city dialog
    @SuppressLint("InflateParams")
    fun showAddCityDialog() {
        val context = this
        val builder = AlertDialog.Builder(context)
        builder.setTitle(R.string.add_city)

        val view = layoutInflater.inflate(R.layout.dialog_add_city, null)
        builder.setView(view)
        builder.setPositiveButton(android.R.string.ok) { dialog, _ ->
            val newCity = view.city.text.toString()
            var isValid = true

            if (newCity.isBlank()) {
                view.city.error = getString(R.string.validation_empty)
                isValid = false
            }

            if (isValid) {
                // Insert city
                val city = City(name = newCity)
                viewModel.insertCity(city)
            } else {
                dialog.dismiss()
            }
        }

        builder.setNegativeButton(android.R.string.cancel) { dialog, _ ->
            dialog.cancel()
        }

        builder.show()
    }
}
