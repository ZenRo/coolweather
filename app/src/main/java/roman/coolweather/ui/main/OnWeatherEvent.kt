package roman.coolweather.ui.main

import roman.coolweather.data.db.entity.City
import roman.coolweather.ui.map.MapInfo

interface OnWeatherEvent {
    fun onWeatherClicked(mapInfo: MapInfo)
    fun onDeleteCityClicked(city: City)
}