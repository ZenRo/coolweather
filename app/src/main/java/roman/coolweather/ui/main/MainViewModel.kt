package roman.coolweather.ui.main

import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import roman.coolweather.data.WeatherRepository
import roman.coolweather.data.db.entity.City
import roman.coolweather.data.db.entity.WeatherDB
import roman.coolweather.util.WeatherMapper

class MainViewModel(private val repository: WeatherRepository) : ViewModel() {
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val weather = MutableLiveData<WeatherDB>()
    val error = MutableLiveData<String?>()

    private fun fetchWeatherAt(city: City) {
        compositeDisposable.add(
            repository.getWeatherAt(city)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                        val w = response
                        w?.city = city // bind local city to weather

                        weather.value = w
                }, { e ->
                    e.printStackTrace()
                })
        )
    }

    fun fetchWeatherAt(lat: String, lon: String) {
        compositeDisposable.add(
            repository.getWeatherAt(lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ w ->
                    if (w.isSuccessful) {
                        val weatherDb = WeatherMapper.weatherToWeatherDb(w.body())
                        repository.insertWeather(weatherDb).subscribe()

                        weather.value = weatherDb
                    } else {
                        error.postValue(w.message())
                    }
                }, { e ->
                    e.printStackTrace()
                })
        )
    }

    fun insertCity(city: City) {
        compositeDisposable.add(repository
            .insertCity(city)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ _ ->
                fetchWeatherAt(city)
            }, { e ->
                e.printStackTrace()
            })
        )
    }

    fun fetchCities() {
        compositeDisposable.add(repository
            .fetchCities()
            .flattenAsFlowable { it }
            .filter { !TextUtils.isEmpty(it.name) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ city ->
                fetchWeatherAt(city)
            }, { e ->
                e.printStackTrace()
            })
        )
    }

    fun deleteCity(city: City) {
        repository.deleteCity(city)
            .subscribeOn(Schedulers.io())
            .subscribe()
    }
}