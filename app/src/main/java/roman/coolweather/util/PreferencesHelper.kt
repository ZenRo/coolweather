package roman.coolweather.util

import android.content.Context

private const val APP_PREFERENCES = "app_preferences"
private const val PREF_FIRST_START = "PREF_FIRST_START"


class PreferencesManager(context: Context) {
    private val settings = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)

    fun setFirstStart(firstStart: Boolean) {
        val editor = settings.edit()
        editor.putBoolean(PREF_FIRST_START, firstStart)
        editor.apply()
    }

    fun isFirstStart(): Boolean {
        return settings.getBoolean(PREF_FIRST_START, true)
    }
}