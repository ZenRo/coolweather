package roman.coolweather.util

import roman.coolweather.data.api.model.Weather
import roman.coolweather.data.db.entity.WeatherDB

class WeatherMapper {
    companion object {
        fun weatherToWeatherDb(weather: Weather?): WeatherDB {
            return WeatherDB(name  = weather?.name ?: "Unknown",
                    country = weather?.sys?.country,
                    temp = weather?.main?.temp?.toString() ?: "0.0",
                    theme = weather?.weather?.first()?.main,
                    description = weather?.weather?.first()?.description,
                    humidity = weather?.main?.humidity ?: 0,
                    lat = weather?.coord?.lat ?: 0.0,
                    lon = weather?.coord?.lon ?: 0.0
                )
        }
    }
}