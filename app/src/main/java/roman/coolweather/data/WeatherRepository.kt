package roman.coolweather.data

import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import roman.coolweather.data.api.WeatherApi
import roman.coolweather.data.api.model.Weather
import roman.coolweather.data.db.AppDatabase
import roman.coolweather.data.db.entity.City
import roman.coolweather.data.db.entity.WeatherDB
import roman.coolweather.util.WeatherMapper


class WeatherRepository(private val weatherClient: WeatherApi, private val database: AppDatabase) {

    fun getWeatherAt(city: City): Flowable<WeatherDB> {
        // Update weather from Internet
        getWeatherAtOnline(city)
            .subscribeOn(Schedulers.io())
            .subscribe()

        // Show cached weather from DB
        return database
            .weatherDao()
            .findByCityName(city.name)
        }

    private fun getWeatherAtOnline(city: City): Maybe<WeatherDB> {
        return Maybe.create<WeatherDB> { w ->
            weatherClient.getWeatherAt(city.name)
                .subscribeOn(Schedulers.io())
                .subscribe({ response ->
                    if (response.isSuccessful) {
                        response.body()?.let {
                            // Add weather to DB
                            val weather = WeatherMapper.weatherToWeatherDb(it)
                            insertWeather(weather).subscribe()
                            w.onSuccess(weather)
                        }
                    } else {
                        // Remove unfound cities
                        if (response.code() == 404) {
                            deleteCity(city).subscribeOn(Schedulers.io()).subscribe()
                        }
                    }
                }, {
                    it.printStackTrace()
                })
        }
    }

    fun getWeatherAt(lat: String, lon: String): Single<Response<Weather>> {
        return weatherClient.getWeatherAt(lat, lon)
    }

    fun insertWeather(weatherDb: WeatherDB): Single<Long> {
        return database.weatherDao().insert(weatherDb).subscribeOn(Schedulers.io())
    }

    fun insertCity(city: City): Single<Long> {
        return database.cityDao().insert(city)
    }

    fun fetchCities(): Single<List<City>> {
        return database.cityDao().getAll()
    }

    fun deleteCity(city: City): Single<Int> {
        return database.cityDao().delete(city)
    }
}