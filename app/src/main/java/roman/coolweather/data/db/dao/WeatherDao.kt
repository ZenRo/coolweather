package roman.coolweather.data.db.dao

import androidx.room.*
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import roman.coolweather.data.db.entity.WeatherDB

@Dao
interface WeatherDao {
    @Query("SELECT * FROM weather_list")
    fun getAll(): Maybe<List<WeatherDB>>

    @Query("SELECT * FROM weather_list WHERE name LIKE :city")
    fun findByCityName(city: String): Flowable<WeatherDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(weather: WeatherDB): Single<Long>

    @Delete
    fun delete(weather: WeatherDB): Single<Int>

    @Update
    fun update(weather: WeatherDB): Single<Int>
}