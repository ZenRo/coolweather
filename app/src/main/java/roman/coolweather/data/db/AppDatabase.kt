package roman.coolweather.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import roman.coolweather.data.db.dao.CityDao
import roman.coolweather.data.db.dao.WeatherDao
import roman.coolweather.data.db.entity.City
import roman.coolweather.data.db.entity.WeatherDB

@Database(entities = [WeatherDB::class, City::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao
    abstract fun cityDao(): CityDao
}