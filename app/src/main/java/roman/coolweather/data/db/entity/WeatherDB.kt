package roman.coolweather.data.db.entity

import androidx.room.*

@Entity(tableName = "weather_list", indices = [Index(value = ["name"], unique = true)])
data class WeatherDB(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "country") var country: String?,
    @ColumnInfo(name = "temp") var temp: String,
    @ColumnInfo(name = "theme") var theme: String?,
    @ColumnInfo(name = "description") var description: String?,
    @ColumnInfo(name = "humidity") var humidity: Int,
    @ColumnInfo(name = "lat") var lat: Double,
    @ColumnInfo(name = "lon") var lon: Double) {

    @Ignore var city: City? = null

    override fun equals(other: Any?): Boolean {
        if (other is WeatherDB) {
            return other.name == name
        }

        return false
    }

    override fun hashCode(): Int {
        return super.hashCode() + name.length
    }
}