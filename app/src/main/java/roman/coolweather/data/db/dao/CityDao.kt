package roman.coolweather.data.db.dao

import androidx.room.*
import io.reactivex.Single
import roman.coolweather.data.db.entity.City

@Dao
interface CityDao {
    @Query("SELECT * FROM city")
    fun getAll(): Single<List<City>>

    @Query("SELECT * FROM city WHERE name LIKE :title")
    fun findByCityTitle(title: String): Single<City>

    @Insert
    fun insert(city: City): Single<Long>

    @Delete
    fun delete(city: City): Single<Int>

    @Update
    fun update(city: City): Single<Int>
}