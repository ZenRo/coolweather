package roman.coolweather.data.api.core


import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import roman.coolweather.BuildConfig
import java.util.concurrent.TimeUnit


private const val URL = "https://api.openweathermap.org/"

class WeatherClient {

    val requests by lazy { createRetrofitClient() }

    private fun createRetrofitClient(): WeatherRequests {
        val okHttpClient = OkHttpClient()
                .newBuilder()
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(WeatherInterceptor())
                .addInterceptor(mHttpLoggingInterceptor)
                .build()

        return Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
                .create(WeatherRequests::class.java)
    }


    private val mHttpLoggingInterceptor: HttpLoggingInterceptor
        get() {
            val logging = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG) {
                logging.level = HttpLoggingInterceptor.Level.BODY
            } else {
                logging.level = HttpLoggingInterceptor.Level.NONE
            }
            return logging
        }
}
