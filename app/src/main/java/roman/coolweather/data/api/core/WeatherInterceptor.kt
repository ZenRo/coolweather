package roman.coolweather.data.api.core

import okhttp3.Interceptor
import okhttp3.Response

class WeatherInterceptor: Interceptor {

    companion object {
        const val API_KEY = "appid"
        const val UNITS = "units"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val httpURL = originalRequest.url
        val key = "90704b2c9cf03d21e9b6fde8a436fa59"

        val newHttpURLBuilder = httpURL.newBuilder()
        newHttpURLBuilder.addQueryParameter(API_KEY, key)
        newHttpURLBuilder.addQueryParameter(UNITS, "metric")
        val newHttpURL = newHttpURLBuilder.build()

        val newRequest = originalRequest.newBuilder().url(newHttpURL).build()
        return chain.proceed(newRequest)
    }
}