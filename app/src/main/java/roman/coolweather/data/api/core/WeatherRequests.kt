package roman.coolweather.data.api.core

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import roman.coolweather.data.api.model.Weather

interface WeatherRequests {
    @GET("data/2.5/weather")
    fun getWeatherAt(@Query("q") location: String): Single<Response<Weather>>

    @GET("data/2.5/weather")
    fun getWeatherAt(@Query("lat") lat: String, @Query("lon") lon: String): Single<Response<Weather>>
}