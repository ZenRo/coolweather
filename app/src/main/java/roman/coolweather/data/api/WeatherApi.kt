package roman.coolweather.data.api

import io.reactivex.Single
import retrofit2.Response
import roman.coolweather.data.api.core.WeatherRequests
import roman.coolweather.data.api.model.Weather

class WeatherApi(private val requests: WeatherRequests) {

    fun getWeatherAt(location: String): Single<Response<Weather>> {
        return requests.getWeatherAt(location)
    }

    fun getWeatherAt(lat: String, lon: String): Single<Response<Weather>> {
        return requests.getWeatherAt(lat, lon)
    }
}